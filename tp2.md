## Project session 2

#### Where should you be ? 

You started the project. You should already have some code related to the project. If
you follwed the plan I gave, you should at this point have an idea on how to localize the key.
You should have implemented this localization process and maybe have tested it.

Communication inside your group should be completely operationnal. 

#### Solutions or help for the n-1 session

If you do not have any idea/code on how to localize the key, you have a problem.
Please review the technological choices you've made. 

If you have an issue regarding precision on the localization techniques you have chosen then contact
us. Most the the time the response will be: Assume that you have the correct position, go ahead... In this case
you should tell the rest of the group that you have made the choice not the stay blocked in this/by this stage
of the deployment.

#### What should you do in this session ? 

Go to the dark side of the force, enjoy free foods and cookies and conquer the world to be the master of the universe. 

You have to answer these questions: 
- __How to raise an Alert ? and What is the kind of alert ?__ 
- Once you've decided the kind of alert you should create/implement it. 
- You should also provide a case when the alert have to be sent. There are many cases you should implement:
    1- Low battery level, when dsiconnect from recharching panel 
    2- Key moving without a registered user borrowing it
    3- Key not plug into the panel, after 19:00.
    4- These are examples...

#### Tips

There are a lot of ways and conditions to raise an alert. The obvious way is to use sound, with all its flaws. 
The best way, I think is to combine everything you have... 

