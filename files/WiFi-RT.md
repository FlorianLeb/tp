### RT:
- RT-WF-Admin `80:2a:a8:59:5c:68`
- RT-WF-PJD `80:2a:a8:49:ad:26`
- RT-WF-RC `80:2a:a8:49:ad:2e`
- RT-WF-SS `80:2a:a8:49:af:2e`
- RT-WF-TD1 `80:2a:a8:46:ce:a5`
- RT-WF-TD3 `80:2a:a8:49:ac:55`

 
### UR:
- Borne 13	`00:1D:A2:69:92:3C` / Admin
- Borne 14	`00:1D:A2:5C:89:BE` 
- Borne 20	`00:1D:A2:69:8E:9A` / TD2
- Borne 21	`00:1D:A2:69:8E:D2` 
- Borne 36	`00:14:6A:66:D5:F4`
- Borne 37	`00:14:A8:39:2C:B6` / Signaux Sys
- Borne 38	`00:14:A8:39:2D:3E`
- Borne 39	`00:21:55:EF:A8:28`
- Borne 40	`00:07:0E:56:93:80` / ProjDoc
- Borne 41	`00:1D:45:26:DD:7A` / Res-cable
- Borne 42	`00:1D:A2:5C:89:CC` / InfoProg
- Borne 53	`00:19:55:AD:64:38`
- Borne 56	`00:13:1A:3A:B5:9C` / TD1
- Borne 57	`00:23:5E:2E:D1:CE`

### exp: 
- Eduspot `00:1D:A2:D8:3D:E1` / GenieInf ( +++ -60dbm)
- Eduspot `00:13:1A:96:7C:D0` / GenieInf ( ++  -65dbm) 
